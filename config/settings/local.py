from .base import *
from .base import env

# GENERAL
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#debug
DEBUG = True
# https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
SECRET_KEY = env(
    "DJANGO_SECRET_KEY",
    default="Wiy0dL3hlWcAq1OuvdHUIAZsMiAjcWIknMNklrNx0iaWorwENMHeBogzNwjaFqne",
)
# https://docs.djangoproject.com/en/dev/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ["localhost", "0.0.0.0", "127.0.0.1"]

# https://django-debug-toolbar.readthedocs.io/en/latest/installation.html#internal-ips
INTERNAL_IPS = ["127.0.0.1", "10.0.2.2"]

# https://docs.celeryq.dev/en/stable/userguide/configuration.html#task-eager-propagates
CELERY_TASK_EAGER_PROPAGATES = True

# DATABASES
# ------------------------------------------------------------------------------
root = Path(__file__).resolve(strict=True).parent.parent.parent
ENV_DIR = root / ".envs/.local/.postgres"
env.read_env(ENV_DIR)

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": env("POSTGRES_DB"),
        "USER": env("POSTGRES_USER"),
        "PASSWORD": env("POSTGRES_PASSWORD"),
        "HOST": env("POSTGRES_HOST"),
    }
}
DATABASES["default"]["ATOMIC_REQUESTS"] = True
DATABASES["default"]["CONN_MAX_AGE"] = env.int("CONN_MAX_AGE", default=60)

# Redis
# ------------------------------------------------------------------------------
root = Path(__file__).resolve(strict=True).parent.parent.parent
ENV_DIR = root / ".envs/.local/.django"
env.read_env(ENV_DIR)
CELERY_BROKER_URL = env("REDIS_URL")

CELERY_TASK_ALWAYS_EAGER = False
