from rest_framework import serializers

from arnika_crawler.crawler.models import CurrencyRateHistory


class CurrencyRateHistorySerializer(serializers.ModelSerializer):
    """ CurrencyRateHistory Serializer
    """

    class Meta:
        model = CurrencyRateHistory
        fields = ["rate", "created_at"]
