from rest_framework import status
from rest_framework.decorators import action
from rest_framework.mixins import ListModelMixin
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from arnika_crawler.crawler.models import CurrencyRateHistory

from .serializers import CurrencyRateHistorySerializer


class CurrencyRateHistoryViewSet(ListModelMixin, GenericViewSet):
    """
    A viewset for viewing and editing user instances.
    """
    serializer_class = CurrencyRateHistorySerializer
    queryset = CurrencyRateHistory.objects.all()

    @action(detail=False, methods=['get'])
    def get_current_usd_rate(self, request, *args, **kwargs):
        """ Get current USD rate

        Returns
        -------
        Response
            Get current USD rate with created_at time

        """
        res = CurrencyRateHistory.objects.last_usd_price()
        data = self.get_serializer(res).data
        return Response(status=status.HTTP_200_OK, data=data)

    @action(detail=False, methods=['get'])
    def get_current_eur_rate(self, request, *args, **kwargs):
        """ Get current EUR rate

        Returns
        -------
        Response
            Get current EUR rate with created_at time
        """
        res = CurrencyRateHistory.objects.last_eur_price()
        data = self.get_serializer(res).data
        return Response(status=status.HTTP_200_OK, data=data)

    @action(detail=False, methods=['get'])
    def get_history_usd_rate(self, request, *args, **kwargs):
        """ Get history USD rate

        Returns
        -------
        Response
            Get history USD rate with created_at time
        """
        res = CurrencyRateHistory.objects.today_usd_price_history()
        data = self.get_serializer(res, many=True).data
        return Response(status=status.HTTP_200_OK, data=data)

    @action(detail=False, methods=['get'])
    def get_history_eur_rate(self, request, *args, **kwargs):
        """ Get history EUR rate

        Returns
        -------
        Response
            Get history EUR rate with created_at time
        """
        res = CurrencyRateHistory.objects.today_eur_price_history()
        data = self.get_serializer(res, many=True).data
        return Response(status=status.HTTP_200_OK, data=data)

    @action(detail=False, methods=['get'])
    def get_usd_eur_rate_history(self, request, *args, **kwargs):
        """ Get USD/EUR rate history

        Returns
        -------
        Response
            Get USD/EUR rate history with created_at time
        """
        res = CurrencyRateHistory.objects.usd_eur_price_history()
        data = self.get_serializer(res, many=True).data
        return Response(status=status.HTTP_200_OK, data=data)
