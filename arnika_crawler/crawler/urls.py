from django.urls import path

from arnika_crawler.crawler.api.views import CurrencyRateHistoryViewSet

app_name = 'crawler'

urlpatterns = [
    path('current/usd/', CurrencyRateHistoryViewSet.as_view({'get': 'get_current_usd_rate'}), name='current_usd'),
    path('current/eur/', CurrencyRateHistoryViewSet.as_view({'get': 'get_current_eur_rate'}), name='current_eur'),
    path("history/usd/", CurrencyRateHistoryViewSet.as_view({'get': 'get_history_usd_rate'}), name='history_usd'),
    path("history/eur/", CurrencyRateHistoryViewSet.as_view({'get': 'get_history_eur_rate'}), name='history_eur'),
    path("history/ratio", CurrencyRateHistoryViewSet.as_view({'get': 'get_usd_eur_rate_history'}), name='history_all'),
]
urls = urlpatterns
