from datetime import datetime

from django.db import models

today = datetime.today()


class Currency(models.TextChoices):
    USD_IRR = 'USD_IRR', 'USD/IRR'
    EUR_IRR = 'EUR_IRR', 'EUR/IRR'
    USD_EUR = 'USD_EUR', 'USD/EUR'


class CurrencyPriceRateManager(models.Manager):
    def create_usd_history(self, **kwargs):
        return self.create(currency=Currency.USD_IRR, **kwargs)

    def create_eur_history(self, **kwargs):
        return self.create(currency=Currency.EUR_IRR, **kwargs)

    def create_usd_eur_history(self, **kwargs):
        return self.create(currency=Currency.USD_EUR, **kwargs)

    def get_usd_history(self):
        return self.get_queryset().filter(currency=Currency.USD_IRR)

    def get_eur_history(self):
        return self.get_queryset().filter(currency=Currency.EUR_IRR).order_by('-created_at').all()

    def get_usd_eur_history(self):
        return self.get_queryset().filter(currency=Currency.USD_EUR).order_by('-created_at').all()

    def last_usd_price(self):
        return self.get_usd_history().order_by('-created_at').first()

    def last_eur_price(self):
        return self.get_eur_history().order_by('-created_at').first()

    def today_usd_price_history(self):
        return self.get_usd_history().filter(created_at__day=today.day).order_by('-created_at').all()

    def today_eur_price_history(self):
        return self.get_eur_history().filter(created_at__day=today.day).order_by('-created_at').all()


class CurrencyRateHistory(models.Model):
    currency = models.CharField(max_length=10, choices=Currency.choices, blank=False, null=False)
    rate = models.FloatField(blank=False, null=False)
    created_at = models.DateTimeField(auto_now_add=True, blank=False, null=False)

    objects = CurrencyPriceRateManager()

    def __str__(self):
        return f'{self.created_at}: {self.currency} =  {self.rate}'
