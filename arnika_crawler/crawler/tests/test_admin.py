import pytest
from django.urls import reverse

pytestmark = pytest.mark.django_db


class TestCurrencyRateHistoryAdmin:
    def test_currency_rate_history_admin_url(self, admin_client):
        url = reverse('admin:crawler_currencyratehistory_changelist')
        response = admin_client.get(url)
        assert response.status_code == 200

    def test_currency_rate_history_admin_add_url(self, admin_client):
        url = reverse('admin:crawler_currencyratehistory_add')
        response = admin_client.get(url)
        assert response.status_code == 200
