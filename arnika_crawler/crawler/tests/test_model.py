from datetime import datetime

import pytest

from arnika_crawler.crawler.models import Currency, CurrencyRateHistory

pytestmark = pytest.mark.django_db

now = datetime.now()


def test_currency_rate_history_model():
    currency_rate_history = CurrencyRateHistory(
        currency=Currency.USD_IRR,
        rate=1.0,
        created_at=now
    )
    currency_rate_history.save()
    assert currency_rate_history.currency == Currency.USD_IRR
    assert currency_rate_history.rate == 1.0


def test_currency_rate_history_model_invalid_rate():
    currency_rate_history = CurrencyRateHistory(
        currency=Currency.USD_IRR,
        rate='',
    )
    with pytest.raises(ValueError):
        currency_rate_history.save()
