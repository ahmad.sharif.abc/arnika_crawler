import pytest
from celery.result import EagerResult

from arnika_crawler.crawler.tasks import get_current_currency_rate, get_current_rate

pytestmark = pytest.mark.django_db


def test_get_usd_rate(settings):
    settings.CELERY_TASK_ALWAYS_EAGER = True

    history_object_result = get_current_currency_rate.delay()
    history_object = history_object_result.result[0]
    assert isinstance(history_object_result, EagerResult)
    assert history_object.rate > 0
    assert type(history_object.rate) == float


def test_get_current_rate(settings):
    usd_url = "https://www.tgju.org/profile/price_dollar_rl"
    eur_url = "https://www.tgju.org/profile/price_eur"
    val = get_current_rate(usd_url)
    assert val > 0
    assert type(val) == float
    val = get_current_rate(eur_url)
    assert val > 0
    assert type(val) == float
