from celery import shared_task

from config import celery_app

from .models import CurrencyRateHistory


def get_current_rate(url):
    import requests
    from bs4 import BeautifulSoup

    r = requests.get(url)

    soup = BeautifulSoup(r.content, 'html')
    val = soup.find('span', {'data-col': 'info.last_trade.PDrCotVal'}).text
    return float(val.replace(',', '.'))


@shared_task
def get_current_currency_rate():
    """ crawl current usd rate from the website https://www.tgju.org/profile/price_dollar_rl"""
    val = get_current_rate("https://www.tgju.org/profile/price_dollar_rl")
    usd_obj = CurrencyRateHistory.objects.create_usd_history(rate=val)

    """ crawl current eur rate from the website https://www.tgju.org/profile/price_eur"""
    val = get_current_rate("https://www.tgju.org/profile/price_eur")
    eur_obj = CurrencyRateHistory.objects.create_eur_history(rate=val)

    val = usd_obj.rate / eur_obj.rate
    CurrencyRateHistory.objects.create_usd_eur_history(rate=val)

    return usd_obj, eur_obj
