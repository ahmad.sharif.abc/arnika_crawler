from django.apps import AppConfig


class CrawlerConfig(AppConfig):
    name = 'arnika_crawler.crawler'
    verbose_name = 'Crawler'
