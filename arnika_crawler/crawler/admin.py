from django.contrib import admin

from .models import *


@admin.register(CurrencyRateHistory)
class CurrencyRateHistoryAdmin(admin.ModelAdmin):
    list_display = ('currency', 'rate')
    list_filter = ('currency',)
    search_fields = ('currency',)
