# Arnika_crawler

Dollar and euro price crawler

## Run for development

1. run postgresql database server and redis server

```shell
docker-compose -f local.yml up -d postgres redis
```

2. run celery worker and beat

```shell
celery -A config.celery_app worker --loglevel=info
celery -A config.celery_app beat --loglevel=info
```

3. if you are in first time:
    1. run `python manage.py makemigrations`
    2. run `python manage.py migrate`
5. start server

```shell
python3 manage.py runserver
```

## Run for production
```shell
docker-compose -f production.yml up -d
```

## Make Documentation

```shell
docker-compose -f local.yml up -d docs
```

# Documantation

- `arnika_crawler` dir is contain main code of crawler. It means django app.
- `compose` dir is contaon docker tools.
- `config` dir is contain all config file and setting in tree mode: local(development), production, test
- `docs` is contain docs file if you make docs with sphinx in docker-compose service.
- `requirements` is contain requirements for diffrent modes.
